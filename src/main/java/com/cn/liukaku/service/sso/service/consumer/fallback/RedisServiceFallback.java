package com.cn.liukaku.service.sso.service.consumer.fallback;

import com.cn.liukaku.common.constants.HttpStatusConstants;
import com.cn.liukaku.common.dto.BaseResult;
import com.cn.liukaku.common.hystrix.Fallback;
import com.cn.liukaku.common.utils.MapperUtils;
import com.cn.liukaku.service.sso.service.consumer.RedisService;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Component;


@Component
public class RedisServiceFallback implements RedisService {

    @Override
    public String put(String key, String value, long seconds) {

        //20200626之前
        //return Fallback.badGateWay();
        return null;
    }

    @Override
    public String get(String key) {
        //20200626之前
        //return Fallback.badGateWay();

        return null;
    }
}
