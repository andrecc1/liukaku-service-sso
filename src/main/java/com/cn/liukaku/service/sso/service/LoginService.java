package com.cn.liukaku.service.sso.service;

import com.cn.liukaku.common.domain.LiukakuUser;

/**
 * 登录
 */
public interface LoginService {

    public LiukakuUser login(String loginCode,String plantPassword);
}
