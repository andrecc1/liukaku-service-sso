package com.cn.liukaku.service.sso.controller;

import com.cn.liukaku.common.domain.LiukakuUser;
import com.cn.liukaku.common.utils.CookieUtils;
import com.cn.liukaku.common.utils.MapperUtils;
import com.cn.liukaku.service.sso.service.LoginService;
import com.cn.liukaku.service.sso.service.consumer.RedisService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

@Controller
public class LoginController {

    @Resource
    private LoginService loginService;

    @Resource
    private RedisService redisService;

    /**
     * 跳转登录页
     *
     * @return
     */
    @RequestMapping(value = "login", method = RequestMethod.GET)
    public String login(@RequestParam(required = false) String url, HttpServletRequest request, Model model) {

        String token = CookieUtils.getCookieValue(request, "token");

        if (StringUtils.isNotBlank(token)) {
            String loginCode = redisService.get(token);

            if (StringUtils.isNotBlank(loginCode)) {
                String json = redisService.get(loginCode);
                if (StringUtils.isNotBlank(json)) {
                    try {
                        LiukakuUser liukakuUser = MapperUtils.json2pojo(json, LiukakuUser.class);

                        if (liukakuUser != null) {
                            if (StringUtils.isNotBlank(url)) {
                                return "redirect:" + url;
                            }
                        }
                        model.addAttribute("liukakuUser", liukakuUser);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        if (StringUtils.isNotBlank(url)) {
            model.addAttribute("url", url);
        }
        return "login";
    }

    @RequestMapping(value = "login", method = RequestMethod.POST)
    public String login(@RequestParam(required = true) String loginCode,
                        @RequestParam(required = true) String password,
                        @RequestParam(required = false) String url,
                        HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
        LiukakuUser liukakuUser = loginService.login(loginCode, password);
        System.out.println(liukakuUser);

        if (liukakuUser == null) {
            redirectAttributes.addFlashAttribute("message", "用户名或者密码错误。");
            return "login";
        }
        if (!liukakuUser.getPassword().equals(password)) {
            redirectAttributes.addFlashAttribute("message", "用户名或者密码错误。");
            return "login";
        }

        String token = UUID.randomUUID().toString();

        String result = redisService.put(token, loginCode, 60 * 60 * 24);

        if ("ok".equals(result)) {
            CookieUtils.setCookie(request, response, "token", token, 60 * 60 * 24);
            if (StringUtils.isNotBlank(url)) {
                return "redirect:" + url;
            }
        } else {
            redirectAttributes.addFlashAttribute("message", "服务器异常稍后再试。");
        }
//        return "redirect:/login";
        return "redirect:/login";
    }

    /**
     * ajax
     * 20200626前
     */
    @ResponseBody
    @RequestMapping(value = "loginAjax", method = RequestMethod.POST)
    public String loginAjax(@RequestParam(required = true) String loginCode,
                            @RequestParam(required = true) String password,
                            @RequestParam(required = false) String url,
                            HttpServletRequest request, HttpServletResponse response, Model model) {
        LiukakuUser liukakuUser = loginService.login(loginCode, password);
        System.out.println(liukakuUser);

        if (liukakuUser == null) {
            model.addAttribute("message", "用户名或者密码错误。");
        }
        if (!liukakuUser.getPassword().equals(password)) {
            model.addAttribute("message", "用户名或者密码错误。");
        }

        if (liukakuUser != null) {
            String token = UUID.randomUUID().toString();

            String result = redisService.put(token, loginCode, 60 * 60 * 24);

            if (StringUtils.isBlank(result) && "ok".equals(result)) {
                CookieUtils.setCookie(request, response, "token", token, 60 * 60 * 24);
                if (StringUtils.isBlank(url)) {
                    return "redirect:" + url;
                }
            }
        } else {
            model.addAttribute("message", "服务器异常稍后再试。");
        }
        return "login";
    }

    /**
     * 单点注销
     * @param url
     * @param request
     * @param response
     * @param model
     * @return
     */
    @RequestMapping(value = "logOut", method = RequestMethod.GET)
    public String logOut(@RequestParam(required = false) String url,
                         HttpServletRequest request, HttpServletResponse response, Model model) {

        CookieUtils.deleteCookie(request, response, "token");

        return login(url, request, model);
    }
}
