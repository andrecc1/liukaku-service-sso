package com.cn.liukaku.service.sso.mapper;

import com.cn.liukaku.common.domain.LiukakuUser;

import java.util.List;

public interface LiukakuUserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(LiukakuUser record);

    LiukakuUser selectByPrimaryKey(Integer id);

    List<LiukakuUser> selectAll();

    int updateByPrimaryKey(LiukakuUser record);
}