package com.cn.liukaku.service.sso.service.impl;

import com.cn.liukaku.common.domain.LiukakuUser;
import com.cn.liukaku.common.utils.MapperUtils;
import com.cn.liukaku.service.sso.mapper.LiukakuUserMapper;
import com.cn.liukaku.service.sso.service.LoginService;
import com.cn.liukaku.service.sso.service.consumer.RedisService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class LoginServiceImpl implements LoginService {

    private static final Logger logger = LoggerFactory.getLogger(LoginServiceImpl.class);

    @Resource
    private LiukakuUserMapper liukakuUserMapper;

    @Resource
    private RedisService redisService;

    @Override
    public LiukakuUser login(String loginCode, String plantPassword) {
        LiukakuUser liukakuUser = null;
        String json = redisService.get(loginCode);
        if (json == null) {
            int id = Integer.parseInt(loginCode);
            liukakuUser = liukakuUserMapper.selectByPrimaryKey(id);
            if (liukakuUser.getPassword().equals(plantPassword)) {
                try {
                    redisService.put(loginCode, MapperUtils.obj2json(liukakuUser), 60 * 60 * 24);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return liukakuUser;
            } else {
                return null;
            }
        } else {
            try {
                liukakuUser = MapperUtils.json2pojo(json, LiukakuUser.class);
            } catch (Exception e) {
                logger.warn("触发了熔断: {}",e.getMessage());
            }
        }
        return liukakuUser;
    }
}
